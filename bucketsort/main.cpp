#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include <ctime>
#include "math.h"

using std::vector;
// functie van de bucket sort
void bucket_sort(vector<int>& list) {

    int grootst = *max_element(list.begin(), list.end()); //haalt het grootste element de range van de lijst
    int kleinst = *min_element(list.begin(), list.end()); //haalt het kleinste element uit de range van de lijst
    int hoevaak_grootst = 1+floor(log10(grootst)); // haalt uit de variabele grootst de hoeveelheid digits. ook wel de som van digits
    int hoevaak_kleinst = 1+ceil(log10(abs(kleinst))); // haalt de negatieve integers uit de lijst. De negatieve
                                                                // de negatieve integers worden met de absoluut functie positief gemaakt.
    int hoevaak = std::max(hoevaak_grootst, hoevaak_kleinst); //haalt de maximale positieve en negatieve integers uit de lijst

    for (int i = 0; i < hoevaak; ++i) {

        vector<vector<int>> buckets = {{}, // 0   1 // Het initaliseren van 18 lege buckets.
                                       {}, // 1   2
                                       {}, // 2   3
                                       {}, // 3   4
                                       {}, // 4   5
                                       {}, // 5   6
                                       {}, // 6   7
                                       {}, // 7   8
                                       {}, // 8   9
                                       {}, // 9  10
                                       {}, // 10 -9
                                       {}, // 11 -8
                                       {}, // 12 -7
                                       {}, // 13 -6
                                       {}, // 14 -5
                                       {}, // 15 -4
                                       {}, // 16 -3
                                       {}, // 17 -2
                                       {}, // 18 -1
                                       };

        // De distribution pass zorgt ervoor dat de elementen in een buckets worden verdeeld. De lijst met integers worden
        // gesorteerd op het meest rechtse digit. De getallen worden 9 bucket plaatsen verschoven waardoor je een
        // verdeling krijgt van hoogste negatieve getallen tot hoogste positieve getallen.
        for (auto l: list) {
            int which_bucket = l / (int) pow(10 ,i) % 10;
            buckets[which_bucket + 9].push_back(l); // indexeren van een bucket e.
        }
        // de getallen in de buckets worden uit de bucket verwijderd.
        list.clear();

        // De gathering pass zorgt er voor dat de lijst wordt gesorteert op het meest linker digit.
        for (auto bucket: buckets) {
            list.insert(list.end(), bucket.begin(), bucket.end()); // geeft de positie aan waar de getallen.
            // in de bucket worden toegevoegd.
            bucket.clear(); // leegt de bucket
        }
    }
}
int main() {

    std::srand(unsigned(std::time(nullptr)));
    std::vector<int> bucket_list_sort(100000); // initaliseert een vector met 100000 elementen.
    std::generate(bucket_list_sort.begin(), bucket_list_sort.end(), []() { return std::rand() % 200000 - 100000; }); // genereert een range van -50000 en 50000 in een lambda functie


     //loop door lijst heen en geeft een 'niet gelukt' aan als het element van bucket_sort list groter als het element wat er naast komt.
    for (int i = 0; i < bucket_list_sort.size(); ++i) {
        if (!(bucket_list_sort[i] <= bucket_list_sort[i+1])) {
            std::cout << "Oepsie:" << bucket_list_sort[i] << " > " << bucket_list_sort[i+1] << std::endl;
            break;
        }
    }

    return 0;
}
