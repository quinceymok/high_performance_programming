//
// Created by quin on 12/02/2022.
//

#include "persoon.h"
#include "game.h"

using std::endl, std::cout;

Persoon::Persoon(std::string naam, double budget) : naam(naam), budget(budget) {

}

void Persoon::koop_game(Game g) {
    for (auto i : games) {
        std::cout << i.get_naam();
    }
    if (!has_game(g) & budget >= g.get_prijs()) {
        games.push_back(g);
        budget -= g.get_nieuw_prijs();
        cout << "gelukt" << endl;
    } else {
        cout << "niet gelukt" << endl;
    }
}

void Persoon::verkoop_game(Game g, Persoon& koper) {
    if (!koper.has_game(g) && has_game(g)) {
        if (koper.get_budget() >= g.get_prijs()) {
            koper.koop_game(g);
            auto begin = std::remove(games.begin(), games.end(), g);
            games.erase(begin, games.end());
            budget += g.get_prijs();
        }
    }
}

double Persoon::get_budget() const {
    return budget ;
}

bool Persoon::has_game(Game g) {
    // checker of Persoon game heeft
    return std::find(games.begin(), games.end(), g) != games.end();
}

void Persoon::set_budget(double budget) {
    budget = budget;
}


std::ostream& operator<<(std::ostream& os, const Persoon& mc)
{
    os << endl << "p1 : " << mc.naam << "heeft een budget van : " << mc.get_budget() <<
 "en bezit de volgende games: "; // << mc.games;
     for (Game g : mc.games) {
         cout << g;
     }
return os;

}

