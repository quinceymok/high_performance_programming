//
// Created by quin on 12/02/2022.
//

#include "game.h"

Game::Game(std::string naam_game, int release_year, double prijs) : naam_game(naam_game),
release_year(release_year), nieuw_prijs(prijs)  {

}
// accessor
std::string Game::get_naam() const {
    return naam_game;
}

double Game::get_prijs() const {
    return nieuw_prijs;
}
double Game::get_nieuw_prijs() const{
    return nieuw_prijs;
}
// mutator
void Game::set_huidige_prijs(int huidige_prijs) {
    huidige_prijs = huidige_prijs;
}

bool operator==(const Game a, const Game b) {
    return a.naam_game == b.naam_game;
}

std::ostream& operator<<(std::ostream& os, const Game& mc) {
    os << mc.get_naam() << " uitgegeven in : " /* << mc.get_release_year() */ << "nieuwe prijs : " <<
       mc.get_prijs() << "nu voor : " << mc.get_nieuw_prijs();
    return os;

}