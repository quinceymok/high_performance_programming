#include <iostream>
#include <vector>
#include <math.h>


std::vector<bool> zeef_van_eratosthenes(int n) {
    std::vector<bool> priemgetal(n+1, true);
    priemgetal[0] = false;
    priemgetal[1] = false;

    // start bij getal 2
    // stel de vermenigvuldigingen van i als niet-priemgetal
    int sqrt_n = floor(sqrt((n)));
    for (int i = 2; i <= n; i++) {
        if (priemgetal[i]) {
            for (int j = i * i; j <= n; j+=i) {
                priemgetal[j] = false;
            }
        }
    }
    return priemgetal;
}

int main() {
    int n = 100;
    int i;
    std::vector<bool> result = zeef_van_eratosthenes(n);
    std::cout <<"Priemgetallen tot aan" << " " << n << " is :\n";

    for (i = 0 ; i < result.size(); i++) {
        if (result[i] == true) {
            std::cout << i << " ";
        }
    }
    return 0;
}