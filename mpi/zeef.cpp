#include <iostream>
#include <vector>
#include <math.h>
#include <algorithm>
#include <chrono>
#include <omp.h>

using namespace std::chrono;

std::vector<bool> zeef_van_eratosthenes(int n) {
    std::vector<bool> priemgetal(n+1, true);
    priemgetal[0] = false;
    priemgetal[1] = false;

    // start bij getal 2
    // stel de vermenigvuldigingen van i als niet-priemgetal
    int sqrt_n = floor(sqrt((n)));
    for (int i = 2; i <= sqrt_n; i++) {
        if (priemgetal[i]) {
            
            for (int j = i * i; j <= n; j+=i) { // more efficient way to multiply with the element in the array than with 2.
                priemgetal[j] = false;
            }

        }
    }
    return priemgetal;
}

int main() {
    int n = 1000000; // number of elements
    int i = 0;

    auto start = high_resolution_clock::now(); //compile runtime

    std::vector<bool> result = zeef_van_eratosthenes(n);

    auto stop = high_resolution_clock::now();

    std::cout <<"Priemgetallen tot aan" << " " << n << " is :\n";

    for (int j = 0 ; j < result.size(); j++) {
        if (result[j] == true) {
            i++;
            std::cout << j << " ";
        }
    }

    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "\n" << duration.count() << " microseconds" << " : " << i << " elementen" << std::endl;

    return 0;
}