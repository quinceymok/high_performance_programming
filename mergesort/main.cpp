#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include "math.h"
#include <chrono>

using namespace std::chrono;
using std::vector;

// Deze functie zorgt ervoor dat de sub-vectoren met elkaar worden gemerged.
void merge(vector<int> &v, int index_links, int index_midden, int index_rechts) {
    int i;
    int j;
    int k;

    //initialiseert linkerkant van de vector
    int lengte_links = index_midden - index_links + 1;
    //initialiseert rechterkant van de vector
    int lengte_rechts = index_rechts - index_midden;
    vector<int> vector_links(lengte_links);
    vector<int> vector_rechts(lengte_rechts);
    // subvectoren worden met elkaar vergeleken  en worden gesorteerd in een nieuwe vector.
    for(i = 0; i < lengte_links; i++) {
        vector_links[i] = v[index_links + i];
    }
    for(j = 0; j < lengte_rechts; j++) {
        vector_rechts[j] = v[index_midden + 1 + j];
    }
    i = 0;
    j = 0;
    k = index_links;

    while ((i < lengte_links) && (j < lengte_rechts)) {
        if(vector_links[i] <= vector_rechts[j]) {
            v[k] = vector_links[i];
            i++;
        } else {
            v[k] = vector_rechts[j];
            j++;
        }
        k++;
    }
    while (i < lengte_links) {
        v[k] = vector_links[i];
        i++;
        k++;
    }
    while (j < lengte_rechts) {
        v[k] = vector_rechts[j];
        j++;
        k++;
    }
}

void merge_sort(vector<int> &v, int index_links, int index_rechts) {
    int index_midden;
    if(index_links < index_rechts) {
        index_midden = (index_links + index_rechts) / 2;
        merge_sort(v, index_links, index_midden);
        merge_sort(v, index_midden+1, index_rechts);
        merge(v, index_links, index_midden, index_rechts);
    }
}
void print_vector(const vector<int> & v) {
    for (auto i: v) {
        std::cout << i << " - ";
    }
    std::cout << std::endl;
}


int main() {
    std::vector<int> vector_1(1000); // initaliseert een vector met 1000 elementen.
    std::generate(vector_1.begin(), vector_1.end(), []() { return std::rand() % 1000; });
    auto start = high_resolution_clock::now();

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);

    std::cout << "Ongesorteerd : ";
    print_vector(vector_1);
    merge_sort(vector_1, 0, vector_1.size()-1);
    std::cout << "Gesorteerd : ";
    print_vector(vector_1);

    std::cout << "Runtime Merge Sort met 1000 elementen: "
              << duration.count() << " microseconds" << std::endl;

    std::vector<int> vector_2(10000); // initaliseert een vector met 1000 elementen.
    std::generate(vector_2.begin(), vector_2.end(), []() { return std::rand() % 1000; });
    start = high_resolution_clock::now();
    std::sort(vector_2.begin(), vector_2.end());

    stop = high_resolution_clock::now();
    duration = duration_cast<microseconds>(stop - start);

    std::cout << "Runtime Merge Sort met 10.000 elementen: "
              << duration.count() << " microseconds" << std::endl;

    std::vector<int> vector_3(30000); // initaliseert een vector met 1000 elementen.
    std::generate(vector_3.begin(), vector_3.end(), []() { return std::rand() % 1000; });
    start = high_resolution_clock::now();
    std::sort(vector_3.begin(), vector_3.end());

    stop = high_resolution_clock::now();
    duration = duration_cast<microseconds>(stop - start);

    std::cout << "Runtime Merge Sort met 30.000 elementen: "
              << duration.count() << " microseconds" << std::endl;

    return 0;
}



