#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include "math.h"
#include <chrono>
#include<thread>
#include <future>
#include <iterator>

using namespace std::chrono;
using std::vector;

// Deze functie zorgt ervoor dat de sub-vectoren met elkaar worden gemerged.
void merge(vector<int> &v, int index_links, int index_midden, int index_rechts) {
    int i;
    int j;
    int k;

    //initialiseert linkerkant van de vector
    int lengte_links = index_midden - index_links + 1;
    //initialiseert rechterkant van de vector
    int lengte_rechts = index_rechts - index_midden;
    vector<int> vector_links(lengte_links);
    vector<int> vector_rechts(lengte_rechts);
    // subvectoren worden met elkaar vergeleken  en worden gesorteerd in een nieuwe vector.
    for(i = 0; i < lengte_links; i++) {
        vector_links[i] = v[index_links + i];
    }
    for(j = 0; j < lengte_rechts; j++) {
        vector_rechts[j] = v[index_midden + 1 + j];
    }
    i = 0;
    j = 0;
    k = index_links;

    while ((i < lengte_links) && (j < lengte_rechts)) {
        if(vector_links[i] <= vector_rechts[j]) {
            v[k] = vector_links[i];
            i++;
        } else {
            v[k] = vector_rechts[j];
            j++;
        }
        k++;
    }
    while (i < lengte_links) {
        v[k] = vector_links[i];
        i++;
        k++;
    }
    while (j < lengte_rechts) {
        v[k] = vector_rechts[j];
        j++;
        k++;
    }
}

void merge_sort(vector<int> &v, int index_links, int index_rechts) {
    int index_midden;
    if(index_links < index_rechts) {

        index_midden = (index_links + index_rechts) / 2;
        merge_sort(v, index_links, index_midden);
        merge_sort(v, index_midden+1, index_rechts);
        merge(v, index_links, index_midden, index_rechts);
    }
}


void merge_sort_mt(vector<int> &v, int index_links, int index_rechts) {
    // In deze functie maken we de merge sort multithreaded. Dat doen we door middel van de chunksize, de iteraties en de
    // verschillende lagen in de merge sort. De verschillende indexen worden berekent door middel van de chunksize die
    // continu door de verschillende lagen gaat. Wanneer er geen lagen en chunksize meer vermenigvuld hij met de aantal
    // iteraties binnen de merge sort.
    int size = v.size(); // Controleert hoeveel elementen in de vector zit
    unsigned int nthreads = std::thread::hardware_concurrency(); // Controleert en veranderd hoeveel threads er mee wordt gegeven
    int chunk_size = size / nthreads;
    vector<std::thread> vec_threads;
    for (int i = 0; i < nthreads ;  i++) {
        int links = i * chunk_size;
        int rechts = (i * chunk_size) + chunk_size - 1;
        std::thread thread = std::thread(merge_sort, std::ref(v), links, rechts);
        vec_threads.push_back(std::move(thread));

    }
    for (auto &t : vec_threads){
        t.join();
    }
    for (int j = 1 ; j <= nthreads / 2 ; j *= 2) {
        for (int i = 0 ; i < nthreads / 2 / j ; i++) {
            int index_links_mt = i * (chunk_size * j) * 2;
            int index_midden_mt = index_links_mt + (chunk_size * j) - 1;
            int index_rechts_mt = index_midden_mt + (chunk_size * j) - 1;
            merge(v, index_links_mt, index_midden_mt,  index_rechts_mt);
        }
    }
}


void print_vector(const vector<int> & v) {
    for (auto i: v) {
        std::cout << i << " - ";
    }
    std::cout << std::endl;
}




int main() {

    for (int i = 1; i <= 10; i++) {
        std::vector<int> test_vec_mt(i * 1000);
        std::generate(test_vec_mt.begin(), test_vec_mt.end(), []() { return std::rand() % 1000; });
        auto start = high_resolution_clock::now();
        merge_sort_mt(test_vec_mt, 0, (i * 1000) - 1);

        auto stop = high_resolution_clock::now();
        auto duration = duration_cast<microseconds>(stop - start);
        std::cout << duration.count() << " microseconds" << " : " << i * 1000 << " elementen" << std::endl;

    }
    return 0;
}
